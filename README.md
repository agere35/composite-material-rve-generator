# Composite Material RVE Generator

This Matlab script generates the .geo and .pro files of a composite material
representative volume element (RVE) with random fibres distribution to be used
in Onelab software Gmsh and GetDP.
Random sequential adsorption algorithm is used to generate the geometry.

INSTRUCTION:
1) RUN "composite_rve_gmsh" script;
2) Input the required data (see example data);
3) Choose if you want fibers that cross the borders case(1) (type 1 on keyboard)
   or not case (2) (type 2);
4) When the process is ended, open on Gmsh the file .geo or .pro that you
   will find on the script's folder;

If "Assigned Volume fraction Vf not reached" error is displayed after RSA
 process, you have to re-launch again the script. Probably the assigned
 volume fraction is too high to be reacheable easily with random approach.
 This script performs good with Vf<0.65 for case (1) and Vf<0.55 for case (2).

File .pro is available ONLY for case (2)