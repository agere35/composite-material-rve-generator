// COMPOSITE MATERIAL RVE WITH RANDOM FIBRE DISTRIBUTION 
// Copyright (c) [2020] [Andrea Geremia] 
// Author: Andrea Geremia 


SetFactory("OpenCASCADE");

// Data 

// Volume fraction of fiber: Vf = 0.6
// Number of fibers: Nf = 20
// Length of the sides of square domain: l = 10.2333
// Radius of fiber: r = 1
// Minimum clearance between two fiber: c = 0.05
h_s = 0.6;  // Mesh density parameter at the external boundaries of domain
h_i = 0.05;  // Mesh density parameter at the interface fiber-matrix
d_min = 0.1;  // Mesh threshold parameter
d_max = 2;  // Mesh parameter


// Square Domain 

Rectangle(1) = {0, 0, 0, 10.2333, 10.2333};


// Fibers 

Disk(2) = {5.5243, 7.4567, 0.0000, 1};
Disk(3) = {4.4599, 4.1402, 0.0000, 1};
Disk(4) = {-0.4134, 0.9249, 0.0000, 1};
Disk(5) = {9.8198, 0.9249, 0.0000, 1};
Disk(6) = {9.8198, 11.1581, 0.0000, 1};
Disk(7) = {10.5322, 3.9917, 0.0000, 1};
Disk(8) = {0.2990, 3.9917, 0.0000, 1};
Disk(9) = {9.1901, 6.5663, 0.0000, 1};
Disk(10) = {1.8426, 10.9854, 0.0000, 1};
Disk(11) = {1.8426, 0.7521, 0.0000, 1};
Disk(12) = {0.9563, 7.2730, 0.0000, 1};
Disk(13) = {11.1896, 7.2730, 0.0000, 1};
Disk(14) = {3.8752, 10.0874, 0.0000, 1};
Disk(15) = {3.8752, -0.1459, 0.0000, 1};
Disk(16) = {8.3078, 9.0128, 0.0000, 1};
Disk(17) = {3.4653, 6.1308, 0.0000, 1};
Disk(18) = {3.4352, 2.2560, 0.0000, 1};
Disk(19) = {6.9287, 3.8793, 0.0000, 1};
Disk(20) = {7.4642, 1.1345, 0.0000, 1};
Disk(21) = {6.2830, 9.4718, 0.0000, 1};
Disk(22) = {6.2830, -0.7615, 0.0000, 1};
Disk(23) = {2.3727, 4.2249, 0.0000, 1};
Disk(24) = {7.1454, 5.9958, 0.0000, 1};
Disk(25) = {5.5469, 1.9490, 0.0000, 1};
Disk(26) = {2.7673, 8.3490, 0.0000, 1};
Disk(27) = {10.4001, 9.1764, 0.0000, 1};
Disk(28) = {0.1668, 9.1764, 0.0000, 1};
Disk(29) = {8.7631, 2.8856, 0.0000, 1};


// Boolean 

BooleanIntersection{ Surface{ 2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29 }; Delete; } { Surface{ 1 }; }
BooleanDifference{ Surface{ 1 }; Delete; } { Surface{ 2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29 }; }


// Mesh Parameters 

Field[1] = Distance;
Field[1].EdgesList = { 5,6,7,9,11,13,15,17,18,20,21,23,24,26,27,29,31,32,34,36,37,38,39,40,41,42,44,46,47,48,49,50,52,53,55,56 };

Field[2] = Threshold;
Field[2].IField = 1;
Field[2].LcMin = h_i;
Field[2].LcMax = h_s;
Field[2].DistMin = d_min;
Field[2].DistMax = d_max;

Field[3] = Box;
Field[3].VIn = h_s;
Field[3].VOut = h_s;
Field[3].XMin = 0;
Field[3].XMax = 10.2333;
Field[3].YMin = 0;
Field[3].YMax = 10.2333;

Field[4] = Min;
Field[4].FieldsList = { 2,3 };
Background Field = 4;

Mesh.CharacteristicLengthFromPoints = 0;
Mesh.CharacteristicLengthFromCurvature = 0;
Mesh.CharacteristicLengthExtendFromBoundary = 0;


// Physical Surface

Physical Surface(1) = {1};  // Matrix
Physical Surface(2) = {2};  // Fiber
Physical Surface(3) = {3};  // Fiber
Physical Surface(4) = {4};  // Fiber
Physical Surface(5) = {5};  // Fiber
Physical Surface(6) = {6};  // Fiber
Physical Surface(7) = {7};  // Fiber
Physical Surface(8) = {8};  // Fiber
Physical Surface(9) = {9};  // Fiber
Physical Surface(10) = {10};  // Fiber
Physical Surface(11) = {11};  // Fiber
Physical Surface(12) = {12};  // Fiber
Physical Surface(13) = {13};  // Fiber
Physical Surface(14) = {14};  // Fiber
Physical Surface(15) = {15};  // Fiber
Physical Surface(16) = {16};  // Fiber
Physical Surface(17) = {17};  // Fiber
Physical Surface(18) = {18};  // Fiber
Physical Surface(19) = {19};  // Fiber
Physical Surface(20) = {20};  // Fiber
Physical Surface(21) = {21};  // Fiber
Physical Surface(22) = {22};  // Fiber
Physical Surface(23) = {23};  // Fiber
Physical Surface(24) = {24};  // Fiber
Physical Surface(25) = {25};  // Fiber
Physical Surface(26) = {26};  // Fiber
Physical Surface(27) = {27};  // Fiber
Physical Surface(28) = {28};  // Fiber
Physical Surface(29) = {29};  // Fiber


// Author: Andrea Geremia   |   Copyright (c) [2020] [Andrea Geremia]   |   https://gitlab.com/agere35
