// COMPOSITE MATERIAL RVE WITH RANDOM FIBRE DISTRIBUTION FOR GMSH 
// Copyright (c) [2020] [Andrea Geremia] 
// Author: Andrea Geremia 


mm = 1.e-3;     // millimeters to meters

// Materials properties

YoungM = 1e9 * DefineNumber[ 200, Name "Matrix/Young modulus [GPa]"];
PoissonM = DefineNumber[ 0.3, Name "Matrix/Poisson coefficient []"];
YoungF = 1e9 * DefineNumber[ 2000, Name "Fiber/Young modulus [GPa]"];
PoissonF = DefineNumber[ 0.2, Name "Fiber/Poisson coefficient []"];

AppliedDisplacement = mm * DefineNumber[ 1, Name "BC/Applied displacement [mm]"];

Scaling = DefineNumber[ 200, Name "Scaling for displacement/Factor []"];



Group {
  // Physical regions: Give explicit labels to the regions defined in the .geo file

  Matrix = Region[1];
  Fiber = Region[{ 2,3,4,5 }];
  Whole = Region[{ 1,2,3,4,5 }];
  LHS = Region[6];
  RHS = Region[7];
  CornerNode = Region [8];

  // Abstract regions:

  Vol_Mec = Region[ Whole ];   // Elastic Domain
  Sur_Clamp_Mec = Region[ LHS ];   // Surface with imposed zero displacements
}

Function {
  E[Matrix] = YoungM;
  E[Fiber] = YoungF;
  nu[Matrix] = PoissonM;
  nu[Fiber] = PoissonF;
}

Function {
  Flag_EPC = 1;
  If(Flag_EPC) // Plane stress
    a[] = E[]/(1.-nu[]^2);
    c[] = E[]*nu[]/(1.-nu[]^2);
  Else // Plane strain or 3D
    a[] = E[]*(1.-nu[])/(1.+nu[])/(1.-2.*nu[]);
    c[] = E[]*nu[]/(1.+nu[])/(1.-2.*nu[]);
  EndIf
  b[] = E[]/2./(1.+nu[]);

  C_xx[] = Tensor[ a[],0  ,0  ,    0  ,b[],0  ,    0  ,0  ,b[] ];
  C_xy[] = Tensor[ 0  ,c[],0  ,    b[],0  ,0  ,    0  ,0  ,0   ];

  C_yx[] = Tensor[ 0  ,b[],0  ,    c[],0  ,0  ,    0  ,0  ,0   ];
  C_yy[] = Tensor[ b[],0  ,0  ,    0  ,a[],0  ,    0  ,0  ,b[] ];
}

// Displacement boundary conditions
Constraint {
  { Name Displacement_x;
    Case {
      { Region Sur_Clamp_Mec ; Type Assign ; Value 0; }
      { Region RHS; Type Assign ; Value AppliedDisplacement; }
    }
  }
  { Name Displacement_y;
    Case {
       { Region CornerNode ; Type Assign ; Value 0; }
    }
  }
}


// Domain of definition of the "ux" and "uy" FunctionSpaces
Group {
  Dom_H_u_Mec = Region[ { Vol_Mec, Sur_Clamp_Mec} ];
}

Flag_Degree = DefineNumber[ 0, Name "Geometry/Use degree 2", Choices{0,1}, Visible 1];
FE_Order = ( Flag_Degree == 0 ) ? 1 : 2; // Convert flag value into polynomial degree

FunctionSpace {
  { Name H_ux_Mec ; Type Form0 ;
    BasisFunction {
      { Name sxn ; NameOfCoef uxn ; Function BF_Node ;
        Support Dom_H_u_Mec ; Entity NodesOf[ All ] ; }
     If ( FE_Order == 2 )
        { Name sxn2 ; NameOfCoef uxn2 ; Function BF_Node_2E ;
          Support Dom_H_u_Mec; Entity EdgesOf[ All ] ; }
     EndIf
    }
    Constraint {
      { NameOfCoef uxn ;
        EntityType NodesOf ; NameOfConstraint Displacement_x ; }
      If ( FE_Order == 2 )
         { NameOfCoef uxn2 ;
	   EntityType EdgesOf ; NameOfConstraint Displacement_x ; }
     EndIf
    }
  }
  { Name H_uy_Mec ; Type Form0 ;
    BasisFunction {
      { Name syn ; NameOfCoef uyn ; Function BF_Node ;
        Support Dom_H_u_Mec ; Entity NodesOf[ All ] ; }
     If ( FE_Order == 2 )
        { Name syn2 ; NameOfCoef uyn2 ; Function BF_Node_2E ;
          Support Dom_H_u_Mec; Entity EdgesOf[ All ] ; }
     EndIf
    }
    Constraint {
      { NameOfCoef uyn ;
        EntityType NodesOf ; NameOfConstraint Displacement_y ; }
      If ( FE_Order == 2 )
      { NameOfCoef uyn2 ;
        EntityType EdgesOf ; NameOfConstraint Displacement_y ; }
      EndIf
    }
  }
}


Jacobian {
  { Name Vol;
    Case {
      { Region All; Jacobian Vol; }
    }
  }
  { Name Sur;
    Case {
      { Region All; Jacobian Sur; }
    }
  }
}

Integration {
  { Name Gauss_v;
    Case {
      If (FE_Order == 1)
      { Type Gauss;
        Case {
          { GeoElement Line       ; NumberOfPoints  3; }
          { GeoElement Triangle   ; NumberOfPoints  3; }
          { GeoElement Quadrangle ; NumberOfPoints  4; }
        }
      }
      Else
      { Type Gauss;
        Case {
	  { GeoElement Line       ; NumberOfPoints  5; }
          { GeoElement Triangle   ; NumberOfPoints  7; }
          { GeoElement Quadrangle ; NumberOfPoints  7; }
        }
      }
      EndIf
    }
  }
}

Formulation {
  { Name Elast_u ; Type FemEquation ;
    Quantity {
      { Name ux  ; Type Local ; NameOfSpace H_ux_Mec ; }
      { Name uy  ; Type Local ; NameOfSpace H_uy_Mec ; }
    }
    Equation {
      Integral { [ -C_xx[] * Dof{d ux}, {d ux} ] ;
        In Vol_Mec ; Jacobian Vol ; Integration Gauss_v ; }
      Integral { [ -C_xy[] * Dof{d uy}, {d ux} ] ;
        In Vol_Mec ; Jacobian Vol ; Integration Gauss_v ; }
      Integral { [ -C_yx[] * Dof{d ux}, {d uy} ] ;
        In Vol_Mec ; Jacobian Vol ; Integration Gauss_v ; }
      Integral { [ -C_yy[] * Dof{d uy}, {d uy} ] ;
        In Vol_Mec ; Jacobian Vol ; Integration Gauss_v ; }
    }
  }
}

Resolution {
  { Name Elast_u ;
    System {
      { Name Sys_Mec ; NameOfFormulation Elast_u; }
    }
    Operation {
      InitSolution [Sys_Mec];
      Generate[Sys_Mec];
      Solve[Sys_Mec];
      SaveSolution[Sys_Mec] ;
    }
  }
}

PostProcessing {
  { Name Elast_u ; NameOfFormulation Elast_u ;
    PostQuantity {
      { Name u ; Value {
          Term { [ Scaling*Vector[ {ux}, {uy}, 0 ]]; In Vol_Mec ; Jacobian Vol ; }
        }
      }
      { Name sig_xx ; Value {
          Term { [ CompX[  C_xx[]*{d ux} + C_xy[]*{d uy} ] ];
            In Vol_Mec ; Jacobian Vol ; }
        }
      }
      { Name sig_xy ; Value {
          Term { [ CompY[  C_xx[]*{d ux} + C_xy[]*{d uy} ] ];
            In Vol_Mec ; Jacobian Vol ; }
        }
      }
      { Name sig_yy ; Value {
          Term { [  CompY [ C_yx[]*{d ux} + C_yy[]*{d uy} ] ];
            In Vol_Mec ; Jacobian Vol ; }
        }
      }
    }
  }
}

PostOperation {
  { Name pos; NameOfPostProcessing Elast_u;
    Operation {
      If(FE_Order == 1)
	Print[ sig_xx, OnElementsOf Whole, File "sigxx.pos" ];
	Print[ sig_xy, OnElementsOf Whole, File "sigxy.pos" ];
        Print[ u, OnElementsOf Whole, File "u.pos" ];
      Else
	Print[ sig_xx, OnElementsOf Whole, File "sigxx2.pos" ];
	Print[ sig_xy, OnElementsOf Whole, File "sigxy2.pos" ];
        Print[ u, OnElementsOf Whole, File "u2.pos" ];
      EndIf
      Echo[ StrCat["l=PostProcessing.NbViews-1; ",
      		   "View[l].VectorType = 5; ",
      		   "View[l].ExternalView = l; ",
      		   "View[l-1].IntervalsType = 3; "
		   ],
             File "tmp.geo", LastTimeStepOnly] ;
    }
  }
}

// Tell Gmsh which GetDP commands to execute when running the model
DefineConstant[
  R_ = {"Elast_u", Name "GetDP/1ResolutionChoices", Visible 0},
  P_ = {"pos", Name "GetDP/2PostOperationChoices", Visible 0},
  C_ = {"-solve -pos -v2 ", Name "GetDP/9ComputeCommand", Visible 0}
];


// Author: Andrea Geremia   |   Copyright (c) [2020] [Andrea Geremia]   |   https://gitlab.com/agere35
