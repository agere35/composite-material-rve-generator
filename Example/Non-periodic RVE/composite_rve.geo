// COMPOSITE MATERIAL RVE WITH RANDOM FIBRE DISTRIBUTION 
// Copyright (c) [2020] [Andrea Geremia] 
// Author: Andrea Geremia 


SetFactory("OpenCASCADE");

// Data 

// Volume fraction of fiber: Vf = 0.55
// Number of fibers: Nf = 4
// Length of the sides of square domain: l = 4.7800
// Radius of fiber: r = 1
// Minimum clearance between two fiber: c = 0.05
h_s = 0.6;  // Mesh density parameter at the external boundaries of domain
h_i = 0.05;  // Mesh density parameter at the interface fiber-matrix
d_min = 0.1;  // Mesh threshold parameter
d_max = 2;  // Mesh parameter


// Square Domain 

Rectangle(1) = {0, 0, 0, 4.7800, 4.7800};


// Fibers 

Disk(2) = {3.6166, 3.1092, 0.0000, 1};
Disk(3) = {1.1916, 1.3796, 0.0000, 1};
Disk(4) = {1.0923, 3.4723, 0.0000, 1};
Disk(5) = {3.2970, 1.0784, 0.0000, 1};


// Boolean 

BooleanIntersection{ Surface{ 2,3,4,5 }; Delete; } { Surface{ 1 }; }
BooleanDifference{ Surface{ 1 }; Delete; } { Surface{ 2,3,4,5 }; }


// Mesh Parameters 

Field[1] = Distance;
Field[1].EdgesList = { 5,6,7,8 };

Field[2] = Threshold;
Field[2].IField = 1;
Field[2].LcMin = h_i;
Field[2].LcMax = h_s;
Field[2].DistMin = d_min;
Field[2].DistMax = d_max;

Field[3] = Box;
Field[3].VIn = h_s;
Field[3].VOut = h_s;
Field[3].XMin = 0;
Field[3].XMax = 4.7800;
Field[3].YMin = 0;
Field[3].YMax = 4.7800;

Field[4] = Min;
Field[4].FieldsList = { 2,3 };
Background Field = 4;

Mesh.CharacteristicLengthFromPoints = 0;
Mesh.CharacteristicLengthFromCurvature = 0;
Mesh.CharacteristicLengthExtendFromBoundary = 0;


// Physical Surface

Physical Surface(1) = {1};  // Matrix
Physical Surface(2) = {2};  // Fiber
Physical Surface(3) = {3};  // Fiber
Physical Surface(4) = {4};  // Fiber
Physical Surface(5) = {5};  // Fiber


// Physical Curve

Physical Curve(6) = {10};  // LHS boundary
Physical Curve(7) = {11};  // RHS boundary


// Physical Point

Physical Point(8) = {9};  // Bottom left corner


// Author: Andrea Geremia   |   Copyright (c) [2020] [Andrea Geremia]   |   https://gitlab.com/agere35
