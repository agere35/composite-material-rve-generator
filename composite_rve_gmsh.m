%% COMPOSITE MATERIAL RVE WITH RANDOM FIBRE DISTRIBUTION FOR GMSH
% Copyright (c) [2020] [Andrea Geremia]
% Author: Andrea Geremia
% Project: https://gitlab.com/agere35/composite-material-rve-generator
%
% Read the LICENSE before use this script
% -------------------------------------------------------------------------
% INSTRUCTION:
% 1) RUN "composite_rve_gmsh" script;
% 2) Input the required data (see example data);
% 3) Choose if you want fibers that cross the borders(type 1 on keyboard) 
%    or not (type 2);
% 4) When the process is ended, open on Gmsh the file .geo or .pro that you
%    will find on the script's folder;
%
% If "Assigned Volume fraction Vf not reached" error is displayed after RSA
% process, you have to re-launch again the script. Probably the assigned
% volume fraction is too high to be reacheable easily with random approach.
% This script performs good with Vf<0.65 for case (1) and Vf<0.55 
% for case (2).
%
% File .pro is available ONLY for case (2) 
% -------------------------------------------------------------------------

%% CLEANER
clc
clf
clear
close all
format short

disp ('%%%%%%%% COMPOSITE MATERIAL RVE WITH RANDOM FIBRE DISTRIBUTION FOR GMSH %%%%%%%%');

%% INPUT DATA

% Example data (see the related file on "Example" folder):
% r=1; c=0.05; Vf=0.60; N_f=20; periodic=1; h_s=0.60; h_i=0.05; d_min=0.1; d_max=2; Ym=200; Pm=0.3; Yf=2000; Pf=0.2; % Periodic RVE
% r=1; c=0.05; Vf=0.55; N_f=4; periodic=2; h_s=0.60; h_i=0.05; d_min=0.1; d_max=2; Ym=200; Pm=0.3; Yf=2000; Pf=0.2; % NON Periodic RVE

% Geometry parameters
r = input('Radius of fiber: r = ');
c = input('Minimum clearance between two fiber: c = ');
Vf = input('Volume fraction of fiber: Vf = ');
if Vf > 0.783
    error('Vf must be < 0.783  (see square packed)')
end
N_f = input('Number of fiber: N_f = ');
l = sqrt(N_f*pi*r^2/Vf);  % Precompute the size of domain
periodic = input('Choose if you want fibers that cross the borders(1) or not(2): ');

% Mesh parameters
h_s = input('Mesh density parameter at the external boundaries of domain: h_s = ');
h_i = input('Mesh density parameter at the interface fiber-matrix: h_i = ');
d_min = input('Mesh de-refinement threshold start parameter: d_min = ');
d_max = input('Mesh de-refinement threshold end parameter: d_max = ');

% Materials properties
if periodic == 2
    Ym = input('Matrix Young modulus [GPa]: Ym = ');
    Pm = input('Matrix Poisson coefficient: Pm = ');
    Yf = input('Fiber Young modulus [GPa]: Yf = ');
    Pf = input('Fiber Poisson coefficient: Pf = ');
end

tic

%% SQUARE DOMAIN

% Square Domain Corner
P(1,:) = [0 0];              % L-L corner
P(2,:) = [l 0];              % L-R corner
P(3,:) = [l l];              % U-R corner
P(4,:) = [0 l];              % U-L corner

%% RANDOM SEQUENTIAL ADSORPTION ALGORITHM

disp ('%%%%%%%% RUNNING RANDOM SEQUENTIAL ADSORPTION ALGORITHM %%%%%%%%');

switch periodic
    case 1      % RVE with fibers that corss the borders
        it = 0; N_fc = 0;                 % While cycle entry conditions
        while N_fc < N_f && it<500000
            x = r+(l-2*r)*rand;           % First fiber random coordinates
            y = r+(l-2*r)*rand;           % First fiber random coordinates
            C(1,:) = [x y];               % Vector of fiber center coordinates
            N_fc = N_fc+1;                % Actual number of fibers
            n_max = 40*(1.5*l)^2;         % Maximum iteration (empirical)
            i = 1; n = 0; col = 0;        % While cycle entry conditions
            while N_fc < N_f && n<n_max
                x = -r+(l+2*r)*rand;      % New fiber random coordinates
                y = -r+(l+2*r)*rand;      % New fiber random coordinates
                % No infraction
                if x>=r && x<=l-r && y>=r && y<=l-r
                    for k=1:i
                        if (2*r)+c >= sqrt((C(k,1)-x)^2+(C(k,2)-y)^2) % Check for collision between fibers
                            col = col+1;
                        end
                    end
                    if col == 0                 % If col=0 then there's no collision between fiber
                        i = i+1;
                        C(i,:) = [x y];         % New fiber center coordinates
                        N_fc = N_fc+1;          % Actual number of fibers
                    end
                % Left infraction
                elseif x<r && y>=r && y<=l-r
                    x1 = x+l;
                    y1 = y;
                    for k=1:i
                        if (2*r)+c >= sqrt((C(k,1)-x)^2+(C(k,2)-y)^2) || (2*r)+c >= sqrt((C(k,1)-x1)^2+(C(k,2)-y1)^2) % Check for collision between fibers
                            col = col+1;
                        end
                    end
                    if col == 0                 % If col=0 then there's no collision between fiber
                        i = i+1;
                        C(i,:) = [x y];         % New fiber center coordinates
                        i = i+1;
                        C(i,:) = [x1 y1];       % New periodic fiber center coordinates
                        N_fc = N_fc+1;          % Actual number of fibers
                    end
                % Right infraction
                elseif x>l-r && y>=r && y<=l-r
                    x1 = x-l;
                    y1 = y;
                    for k=1:i
                        if (2*r)+c >= sqrt((C(k,1)-x)^2+(C(k,2)-y)^2) || (2*r)+c >= sqrt((C(k,1)-x1)^2+(C(k,2)-y1)^2) % Check for collision between fibers
                            col = col+1;
                        end
                    end
                    if col == 0                 % If col=0 then there's no collision between fiber
                        i = i+1;
                        C(i,:) = [x y];         % New fiber center coordinates
                        i = i+1;
                        C(i,:) = [x1 y1];       % New periodic fiber center coordinates
                        N_fc = N_fc+1;          % Actual number of fibers
                    end
                % Lower infraction
                elseif x>=r && x<=l-r && y<r
                    x1 = x;
                    y1 = y+l;
                    for k=1:i
                        if (2*r)+c >= sqrt((C(k,1)-x)^2+(C(k,2)-y)^2) || (2*r)+c >= sqrt((C(k,1)-x1)^2+(C(k,2)-y1)^2) % Check for collision between fibers
                            col = col+1;
                        end
                    end
                    if col == 0                 % If col=0 then there's no collision between fiber
                        i = i+1;
                        C(i,:) = [x y];         % New fiber center coordinates
                        i = i+1;
                        C(i,:) = [x1 y1];       % New periodic fiber center coordinates
                        N_fc = N_fc+1;          % Actual number of fibers
                    end
                % Upper infraction
                elseif x>=r && x<=l-r && y>l-r
                    x1 = x;
                    y1 = y-l;
                    for k=1:i
                        if (2*r)+c >= sqrt((C(k,1)-x)^2+(C(k,2)-y)^2) || (2*r)+c >= sqrt((C(k,1)-x1)^2+(C(k,2)-y1)^2) % Check for collision between fibers
                            col = col+1;
                        end
                    end
                    if col == 0                 % If col=0 then there's no collision between fiber
                        i = i+1;
                        C(i,:) = [x y];         % New fiber center coordinates
                        i = i+1;
                        C(i,:) = [x1 y1];       % New periodic fiber center coordinates
                        N_fc = N_fc+1;          % Actual number of fibers
                    end
                % Lower-Left infraction
                elseif x<r && y<r && ~(x<0 && y<0 && r<sqrt((P(1,1)-x)^2+(P(1,2)-y)^2))
                    x1 = x+l;         % L-R corner
                    y1 = y;           % L-R corner
                    x2 = x+l;         % U-R corner
                    y2 = y+l;         % U-R corner
                    x3 = x;           % U-L corner
                    y3 = y+l;         % U-L corner
                    for k=1:i
                        if (2*r)+c >= sqrt((C(k,1)-x)^2+(C(k,2)-y)^2) || (2*r)+c >= sqrt((C(k,1)-x1)^2+(C(k,2)-y1)^2) || (2*r)+c >= sqrt((C(k,1)-x2)^2+(C(k,2)-y2)^2) || (2*r)+c >= sqrt((C(k,1)-x3)^2+(C(k,2)-y3)^2) % Check for collision between fibers
                            col = col+1;
                        end
                    end
                    if col == 0                 % If col=0 then there's no collision between fiber
                        i = i+1;
                        C(i,:) = [x y];         % New fiber center coordinates
                        if ~(x1>l && y1<0 && r<sqrt((P(2,1)-x1)^2+(P(2,2)-y1)^2))
                            i = i+1;
                            C(i,:) = [x1 y1];   % New periodic L-R corner fiber center coordinates
                        end
                        if ~(x2>l && y2>l && r<sqrt((P(3,1)-x2)^2+(P(3,2)-y2)^2))
                            i = i+1;
                            C(i,:) = [x2 y2];   % New periodic U-R corner fiber center coordinates
                        end
                        if ~(x3<0 && y3>l && r<sqrt((P(4,1)-x3)^2+(P(4,2)-y3)^2))
                            i = i+1;
                            C(i,:) = [x3 y3];   % New periodic U-L corner fiber center coordinates
                        end
                        N_fc = N_fc+1;          % Actual number of fibers
                    end
                % Lower-Right infraction
                elseif x>l-r && y<r && ~(x>l && y<0 && r<sqrt((P(2,1)-x)^2+(P(2,2)-y)^2))
                    x1 = x;           % U-R corner
                    y1 = y+l;         % U-R corner
                    x2 = x-l;         % U-L corner
                    y2 = y+l;         % U-L corner
                    x3 = x-l;         % L-L corner
                    y3 = y;           % L-L corner
                    for k=1:i
                        if (2*r)+c >= sqrt((C(k,1)-x)^2+(C(k,2)-y)^2) || (2*r)+c >= sqrt((C(k,1)-x1)^2+(C(k,2)-y1)^2) || (2*r)+c >= sqrt((C(k,1)-x2)^2+(C(k,2)-y2)^2) || (2*r)+c >= sqrt((C(k,1)-x3)^2+(C(k,2)-y3)^2) % Check for collision between fibers
                            col = col+1;
                        end
                    end
                    if col == 0                 % If col=0 then there's no collision between fiber
                        i = i+1;
                        C(i,:) = [x y];         % New fiber center coordinates
                        if ~(x1>l && y1>l && r<sqrt((P(3,1)-x1)^2+(P(3,2)-y1)^2))
                            i = i+1;
                            C(i,:) = [x1 y1];   % New periodic U-R corner fiber center coordinates
                        end
                        if ~(x2<0 && y2>l && r<sqrt((P(4,1)-x2)^2+(P(4,2)-y2)^2))
                            i = i+1;
                            C(i,:) = [x2 y2];   % New periodic U-L corner fiber center coordinates
                        end
                        if ~(x3<0 && y3<0 && r<sqrt((P(1,1)-x3)^2+(P(1,2)-y3)^2))
                            i = i+1;
                            C(i,:) = [x3 y3];   % New periodic L-L corner fiber center coordinates
                        end
                        N_fc = N_fc+1;          % Actual number of fibers
                    end
                % Upper-Left infraction
                elseif x<r && y>l-r && ~(x<0 && y>l && r<sqrt((P(4,1)-x)^2+(P(4,2)-y)^2))
                    x1 = x;           % L-L corner
                    y1 = y-l;         % L-L corner
                    x2 = x+l;         % L-R corner
                    y2 = y-l;         % L-R corner
                    x3 = x+l;         % U-R corner
                    y3 = y;           % U-R corner
                    for k=1:i
                        if (2*r)+c >= sqrt((C(k,1)-x)^2+(C(k,2)-y)^2) || (2*r)+c >= sqrt((C(k,1)-x1)^2+(C(k,2)-y1)^2) || (2*r)+c >= sqrt((C(k,1)-x2)^2+(C(k,2)-y2)^2) || (2*r)+c >= sqrt((C(k,1)-x3)^2+(C(k,2)-y3)^2) % Check for collision between fibers
                            col = col+1;
                        end
                    end
                    if col == 0                 % If col=0 then there's no collision between fiber
                        i = i+1;
                        C(i,:) = [x y];         % New fiber center coordinates
                        if ~(x1<0 && y1<0 && r<sqrt((P(1,1)-x1)^2+(P(1,2)-y1)^2))
                            i = i+1;
                            C(i,:) = [x1 y1];   % New periodic L-L corner fiber center coordinates
                        end
                        if ~(x2>l && y2<0 && r<sqrt((P(2,1)-x2)^2+(P(2,2)-y2)^2))
                            i = i+1;
                            C(i,:) = [x2 y2];   % New periodic L-R corner fiber center coordinates
                        end
                        if ~(x3>l && y3>l && r<sqrt((P(3,1)-x3)^2+(P(3,2)-y3)^2))
                            i = i+1;
                            C(i,:) = [x3 y3];   % New periodic U-R corner fiber center coordinates
                        end
                        N_fc = N_fc+1;          % Actual number of fibers
                    end
                % Upper-Right infraction
                elseif x>l-r && y>l-r && ~(x>l && y>l && r<sqrt((P(3,1)-x)^2+(P(3,2)-y)^2))
                    x1 = x-l;         % U-L corner
                    y1 = y;           % U-L corner
                    x2 = x-l;         % L-L corner
                    y2 = y-l;         % L-L corner
                    x3 = x;           % L-R corner
                    y3 = y-l;         % L-R corner
                    for k=1:i
                        if (2*r)+c >= sqrt((C(k,1)-x)^2+(C(k,2)-y)^2) || (2*r)+c >= sqrt((C(k,1)-x1)^2+(C(k,2)-y1)^2) || (2*r)+c >= sqrt((C(k,1)-x2)^2+(C(k,2)-y2)^2) || (2*r)+c >= sqrt((C(k,1)-x3)^2+(C(k,2)-y3)^2) % Check for collision between fibers
                            col = col+1;
                        end
                    end
                    if col == 0                 % If col=0 then there's no collision between fiber
                        i = i+1;
                        C(i,:) = [x y];         % New fiber center coordinates
                        if ~(x1<0 && y1>l && r<sqrt((P(4,1)-x1)^2+(P(4,2)-y1)^2))
                            i = i+1;
                            C(i,:) = [x1 y1];   % New periodic U-L corner fiber center coordinates
                        end
                        if ~(x2<0 && y2<0 && r<sqrt((P(1,1)-x2)^2+(P(1,2)-y2)^2))
                            i = i+1;
                            C(i,:) = [x2 y2];   % New periodic L-L corner fiber center coordinates
                        end
                        if ~(x3>l && y3<0 && r<sqrt((P(2,1)-x3)^2+(P(2,2)-y3)^2))
                            i = i+1;
                            C(i,:) = [x3 y3];   % New periodic L-R corner fiber center coordinates
                        end
                        N_fc = N_fc+1;          % Actual number of fibers
                    end
                end
                col = 0;
                n = n+1;            % Number of iterations
            end
            it = it+1;              % Number of attempt
            if N_fc < N_f           % Assigned volume fraction not reached
                clear C
                N_fc = 0;
            end
        end
        
    case 2      % RVE with fibers that not corss the borders
        it = 0; N_fc = 0;                 % While cycle entry conditions
        while N_fc < N_f && it<500000
            x = r+(l-2*r)*rand;
            y = r+(l-2*r)*rand;
            C(1,:) = [x y];               % Vector of fiber center coordinates
            N_fc = N_fc+1;                % Actual number of fibers
            n_max = 40*(1.5*l)^2;         % Maximum iteration (empirical)
            i = 1; n = 0; col = 0;        % While cycle entry conditions
            while N_fc < N_f && n<n_max
                x = r+(l-2*r)*rand;
                y = r+(l-2*r)*rand;
                for k=1:i
                    if (2*r)+c >= sqrt((C(k,1)-x)^2+(C(k,2)-y)^2) % Check for collision between fibers
                        col = col+1;
                    end
                end
                if col == 0               % If col=0 then there's no collision between fiber
                    i = i+1;
                    C(i,:) = [x y];       % New fiber center coordinates
                    N_fc = N_fc+1;        % Actual number of fibers
                end
                col = 0;
                n = n+1;                  % Number of iterations
            end
            it = it+1;                    % Number of attempt
            if N_fc < N_f                 % Assigned volume fraction not reached
                clear C
                N_fc = 0;
            end
        end
end

if N_fc < N_f
    toc
    error('Assigned Volume fraction Vf not reached... TRY AGAIN')
end

%% CREATING FILE GEO FOR GMSH

disp ('%%%%%%%% CREATING FILE GEO FOR GMSH %%%%%%%%');

geo = fopen ('composite_rve.geo', 'w');
fprintf(geo, '%s\n', '// COMPOSITE MATERIAL RVE WITH RANDOM FIBRE DISTRIBUTION ');
fprintf(geo, '%s\n', '// Copyright (c) [2020] [Andrea Geremia] ');
fprintf(geo, '%s\n\n\n', '// Author: Andrea Geremia ');
fprintf(geo, '%s\n\n', 'SetFactory("OpenCASCADE");');

% Data
fprintf(geo, '%s\n\n', '// Data ');
fprintf(geo, '%s\n', ['// Volume fraction of fiber: Vf = ', num2str(Vf)]);
fprintf(geo, '%s\n', ['// Number of fibers: Nf = ', num2str(N_fc)]);
fprintf(geo, '%s\n', ['// Length of the sides of square domain: l = ', num2str(l,'%.4f')]);
fprintf(geo, '%s\n', ['// Radius of fiber: r = ', num2str(r)]);
fprintf(geo, '%s\n', ['// Minimum clearance between two fiber: c = ', num2str(c,'%.2f')]);
fprintf(geo, '%s\n', ['h_s = ', num2str(h_s), ';  // Mesh density parameter at the external boundaries of domain']);
fprintf(geo, '%s\n', ['h_i = ', num2str(h_i), ';  // Mesh density parameter at the interface fiber-matrix']);
fprintf(geo, '%s\n', ['d_min = ', num2str(d_min), ';  // Mesh threshold parameter']);
fprintf(geo, '%s\n\n\n', ['d_max = ', num2str(d_max), ';  // Mesh threshold parameter']);

% Square Domain
fprintf(geo, '%s\n\n', '// Square Domain ');
fprintf(geo, '%s\n\n\n', ['Rectangle(1) = {0, 0, 0, ', num2str(l,'%.4f'), ', ', num2str(l,'%.4f'),'};']);  % Square definition

% Fibers
fprintf(geo, '%s\n\n', '// Fibers ');
for i=2:size(C,1)+1  % Fibers definition
    fprintf(geo, '%s\n',['Disk(', num2str(i), ') = {', num2str(C(i-1,1),'%.4f'), ', ', num2str(C(i-1,2),'%.4f'),', 0.0000, ', num2str(r), '};']);
end

% Boolean Intersection (Fibers surface definition)
fprintf(geo, '\n\n%s\n\n', '// Boolean ');
fprintf(geo, 'BooleanIntersection{ Surface{ 2');
for i=3:size(C,1)+1
    fprintf(geo, '%s',[',', num2str(i)]);
end
fprintf(geo, '%s\n', ' }; Delete; } { Surface{ 1 }; }');

% Boolean Difference (Matrix surface definition)
fprintf(geo, 'BooleanDifference{ Surface{ 1 }; Delete; } { Surface{ 2');
for i=3:size(C,1)+1
    fprintf(geo, '%s',[',', num2str(i)]);
end
fprintf(geo, '%s\n\n\n', ' }; }');

% Mesh Parameters
fprintf(geo, '%s\n\n', '// Mesh Parameters ');
fprintf(geo, '%s\n', 'Field[1] = Distance;');
k = 5;
fprintf(geo, '%s', ['Field[1].EdgesList = { ', num2str(k)]);
for i=2:size(C,1)
    % No infraction
    if r<=C(i,1) && C(i,1)<=(l-r) && r<=C(i,2) && C(i,2)<=(l-r)
        k = k+1;
        fprintf(geo, '%s',[',', num2str(k)]);
    % Left infraction
    elseif C(i,1)<r && r<=C(i,2) && C(i,2)<=l-r
        k = k+1;
        fprintf(geo, '%s',[',', num2str(k)]);
        k = k+2;
        fprintf(geo, '%s',[',', num2str(k)]);
    % Right infraction
    elseif l-r<C(i,1) && r<=C(i,2) && C(i,2)<=l-r
        k = k+2;
        fprintf(geo, '%s',[',', num2str(k)]);
    % Lower infraction (center on domain)
    elseif r<=C(i,1) && C(i,1)<=l-r && 0<C(i,2) && C(i,2)<r
        k = k+1;
        fprintf(geo, '%s',[',', num2str(k)]);
        k = k+2;
        fprintf(geo, '%s',[',', num2str(k)]);
     % Lower infraction (center out of domain)
    elseif r<=C(i,1) && C(i,1)<=l-r && C(i,2)<=0
        k = k+2;
        fprintf(geo, '%s',[',', num2str(k)]);
    % Upper infraction  (center on domain)
    elseif r<=C(i,1) && C(i,1)<=l-r && l-r<C(i,2) && C(i,2)<l
        k = k+1;
        fprintf(geo, '%s',[',', num2str(k)]);
        k = k+2;
        fprintf(geo, '%s',[',', num2str(k)]);
    % Upper infraction  (center out of domain)
    elseif r<=C(i,1) && C(i,1)<=l-r && C(i,2)>=l
        k = k+2;
        fprintf(geo, '%s',[',', num2str(k)]);

    % Lower-Left infraction (Case 1: [x<r 0<y<r d<r])
    elseif C(i,1)<r && 0<C(i,2) && C(i,2)<r && sqrt((P(1,1)-C(i,1))^2+(P(1,2)-C(i,2))^2)<r
        k = k+1;
        fprintf(geo, '%s',[',', num2str(k)]);
        k = k+3;
        fprintf(geo, '%s',[',', num2str(k)]);
    % Lower-Left infraction (Case 2: [x<r y<0 d<r])
    elseif C(i,1)<r && C(i,2)<0 && sqrt((P(1,1)-C(i,1))^2+(P(1,2)-C(i,2))^2)<r
        k = k+3;
        fprintf(geo, '%s',[',', num2str(k)]);
    % Lower-Left infraction (Case 3: [0<x<r 0<y<r d>r])
    elseif 0<C(i,1) && C(i,1)<r && 0<C(i,2) && C(i,2)<r && sqrt((P(1,1)-C(i,1))^2+(P(1,2)-C(i,2))^2)>r
        k = k+1;
        fprintf(geo, '%s',[',', num2str(k)]);
        k = k+2;
        fprintf(geo, '%s',[',', num2str(k)]);
        k = k+2;
        fprintf(geo, '%s',[',', num2str(k)]);
    % Lower-Left infraction (Case 4: [0<x<r y<0 d>r])
    elseif 0<C(i,1) && C(i,1)<r && C(i,2)<0 && sqrt((P(1,1)-C(i,1))^2+(P(1,2)-C(i,2))^2)>r
        k = k+2;
        fprintf(geo, '%s',[',', num2str(k)]);
    % Lower-Left infraction (Case 5: [x<0 0<y<r d>r])
    elseif C(i,1)<0 && 0<C(i,2) && C(i,2)<r && sqrt((P(1,1)-C(i,1))^2+(P(1,2)-C(i,2))^2)>r
        k = k+1;
        fprintf(geo, '%s',[',', num2str(k)]);
        k = k+2;
        fprintf(geo, '%s',[',', num2str(k)]);
    % Lower-Right infraction (Case 1: [l-r<x y<r d<r])
    elseif l-r<C(i,1) && C(i,2)<r && sqrt((P(2,1)-C(i,1))^2+(P(2,2)-C(i,2))^2)<r
        k = k+3;
        fprintf(geo, '%s',[',', num2str(k)]);
    % Lower-Right infraction (Case 2: [l-r<x<l 0<y<r d>r])
    elseif l-r<C(i,1) && C(i,1)<l && 0<C(i,2) && C(i,2)<r && sqrt((P(2,1)-C(i,1))^2+(P(2,2)-C(i,2))^2)>r
        k = k+2;
        fprintf(geo, '%s',[',', num2str(k)]);
        k = k+2;
        fprintf(geo, '%s',[',', num2str(k)]);
    % Lower-Right infraction (Case 3: [l-r<x<l y<0 d>r])
    elseif l-r<C(i,1) && C(i,1)<l && C(i,2)<0 && sqrt((P(2,1)-C(i,1))^2+(P(2,2)-C(i,2))^2)>r
        k = k+2;
        fprintf(geo, '%s',[',', num2str(k)]);
    % Lower-Right infraction (Case 4: [l<x 0<y<r d>r])
    elseif l<C(i,1) && 0<C(i,2) && C(i,2)<r && sqrt((P(2,1)-C(i,1))^2+(P(2,2)-C(i,2))^2)>r
        k = k+2;
        fprintf(geo, '%s',[',', num2str(k)]);
    % Upper-Right infraction (Case 1: [l-r<x l-r<y d<r])
    elseif l-r<C(i,1) && l-r<C(i,2) && sqrt((P(3,1)-C(i,1))^2+(P(3,2)-C(i,2))^2)<r
        k = k+3;
        fprintf(geo, '%s',[',', num2str(k)]);
    % Upper-Right infraction (Case 2: [l-r<x<l l-r<y<l d>r])
    elseif l-r<C(i,1) && C(i,1)<l && l-r<C(i,2) && C(i,2)<l && sqrt((P(3,1)-C(i,1))^2+(P(3,2)-C(i,2))^2)>r
        k = k+2;
        fprintf(geo, '%s',[',', num2str(k)]);
        k = k+2;
        fprintf(geo, '%s',[',', num2str(k)]);
    % Upper-Right infraction (Case 3: [l-r<x<l l<y d>r])
    elseif l-r<C(i,1) && C(i,1)<l && l<C(i,2) && sqrt((P(3,1)-C(i,1))^2+(P(3,2)-C(i,2))^2)>r
        k = k+2;
        fprintf(geo, '%s',[',', num2str(k)]);
    % Upper-Right infraction (Case 4: [l<x l-r<y<l d>r])
    elseif l<C(i,1) && l-r<C(i,2) && C(i,2)<l && sqrt((P(3,1)-C(i,1))^2+(P(3,2)-C(i,2))^2)>r
        k = k+2;
        fprintf(geo, '%s',[',', num2str(k)]);
    % Upper-Left infraction (Case 1: [x<r l-r<y<l d<r])
    elseif C(i,1)<r && l-r<C(i,2) && C(i,2)<l && sqrt((P(4,1)-C(i,1))^2+(P(4,2)-C(i,2))^2)<r
        k = k+1;
        fprintf(geo, '%s',[',', num2str(k)]);
        k = k+3;
        fprintf(geo, '%s',[',', num2str(k)]);
    % Upper-Left infraction (Case 2: [x<r l<y d<r])
    elseif C(i,1)<r && l<C(i,2) && sqrt((P(4,1)-C(i,1))^2+(P(4,2)-C(i,2))^2)<r
        k = k+3;
        fprintf(geo, '%s',[',', num2str(k)]);
    % Upper-Left infraction (Case 3: [0<x<r l-r<y<l d>r])
    elseif 0<C(i,1) && C(i,1)<r && l-r<C(i,2) && C(i,2)<l && sqrt((P(4,1)-C(i,1))^2+(P(4,2)-C(i,2))^2)>r
        k = k+1;
        fprintf(geo, '%s',[',', num2str(k)]);
        k = k+2;
        fprintf(geo, '%s',[',', num2str(k)]);
        k = k+2;
        fprintf(geo, '%s',[',', num2str(k)]);
    % Upper-Left infraction (Case 4: [0<x<r l<y d>r])
    elseif 0<C(i,1) && C(i,1)<r && l<C(i,2) && sqrt((P(4,1)-C(i,1))^2+(P(4,2)-C(i,2))^2)>r
        k = k+2;
        fprintf(geo, '%s',[',', num2str(k)]);
    % Upper-Left infraction (Case 5: [x<0 l-r<y<l d>r])
    elseif C(i,1)<0 && l-r<C(i,2) && C(i,2)<l && sqrt((P(4,1)-C(i,1))^2+(P(4,2)-C(i,2))^2)>r
        k = k+1;
        fprintf(geo, '%s',[',', num2str(k)]);
        k = k+2;
        fprintf(geo, '%s',[',', num2str(k)]);
    end
end
fprintf(geo, '%s\n\n', ' };');

fprintf(geo, '%s\n', 'Field[2] = Threshold;');
fprintf(geo, '%s\n', 'Field[2].IField = 1;');
fprintf(geo, '%s\n', 'Field[2].LcMin = h_i;');
fprintf(geo, '%s\n', 'Field[2].LcMax = h_s;');
fprintf(geo, '%s\n', 'Field[2].DistMin = d_min;');
fprintf(geo, '%s\n\n', 'Field[2].DistMax = d_max;');

fprintf(geo, '%s\n', 'Field[3] = Box;');
fprintf(geo, '%s\n', 'Field[3].VIn = h_s;');
fprintf(geo, '%s\n', 'Field[3].VOut = h_s;');
fprintf(geo, '%s\n', 'Field[3].XMin = 0;');
fprintf(geo, '%s\n', ['Field[3].XMax = ', num2str(l,'%.4f'),';']);
fprintf(geo, '%s\n', 'Field[3].YMin = 0;');
fprintf(geo, '%s\n\n', ['Field[3].YMax = ', num2str(l,'%.4f'),';']);

fprintf(geo, '%s\n', 'Field[4] = Min;');
fprintf(geo, '%s\n', 'Field[4].FieldsList = { 2,3 };');
fprintf(geo, '%s\n\n', 'Background Field = 4;');

% Prevent Over Refinement
fprintf(geo, '%s\n', 'Mesh.CharacteristicLengthFromPoints = 0;');
fprintf(geo, '%s\n', 'Mesh.CharacteristicLengthFromCurvature = 0;');
fprintf(geo, '%s\n\n\n', 'Mesh.CharacteristicLengthExtendFromBoundary = 0;');

 % Physical Surface
fprintf(geo, '%s\n\n', '// Physical Surface');
fprintf(geo, '%s\n','Physical Surface(1) = {1};  // Matrix');
for i=1:size(C,1)
    fprintf(geo, '%s\n',['Physical Surface(', num2str(i+1), ') = {', num2str(i+1), '};  // Fiber']);
end
fprintf(geo, '\n\n');

if periodic == 1
    % Physical Curve
    fprintf(geo, '%s\n\n', '// Physical Curve');
    fprintf(geo, '%s\n',['Physical Curve(', num2str(size(C,1)+2),') = {', num2str(N_f+6),'};  // LHS boundary <- MODIFY HERE']);
    fprintf(geo, '%s\n\n\n',['Physical Curve(', num2str(size(C,1)+3),') = {', num2str(N_f+7),'};  // RHS boundary <- MODIFY HERE']);

    % Physical Point
    fprintf(geo, '%s\n\n', '// Physical Point');
    fprintf(geo, '%s\n\n\n',['Physical Point(', num2str(size(C,1)+4),') = {1};  // Bottom left corner']);
    
elseif periodic == 2
    % Physical Curve
    fprintf(geo, '%s\n\n', '// Physical Curve');
    fprintf(geo, '%s\n',['Physical Curve(', num2str(size(C,1)+2),') = {', num2str(N_f+6),'};  // LHS boundary']);
    fprintf(geo, '%s\n\n\n',['Physical Curve(', num2str(size(C,1)+3),') = {', num2str(N_f+7),'};  // RHS boundary']);

    % Physical Point
    fprintf(geo, '%s\n\n', '// Physical Point');
    fprintf(geo, '%s\n\n\n',['Physical Point(', num2str(size(C,1)+4),') = {', num2str(N_f+5),'};  // Bottom left corner']);
end

fprintf(geo, '%s\n', '// Author: Andrea Geremia   |   Copyright (c) [2020] [Andrea Geremia]   |   https://gitlab.com/agere35');
fclose(geo);

%% CREATING FILE PRO FOR GMSH

disp ('%%%%%%%% CREATING FILE PRO FOR GMSH %%%%%%%%');

pro = fopen ('composite_rve.pro', 'w');
fprintf(pro, '%s\n', '// COMPOSITE MATERIAL RVE WITH RANDOM FIBRE DISTRIBUTION FOR GMSH ');
fprintf(pro, '%s\n', '// Copyright (c) [2020] [Andrea Geremia] ');
fprintf(pro, '%s\n\n\n', '// Author: Andrea Geremia ');

fprintf(pro, '%s\n\n', 'mm = 1.e-3;     // millimeters to meters');
fprintf(pro, '%s\n\n', '// Materials properties');
fprintf(pro, '%s\n', ['YoungM = 1e9 * DefineNumber[ ',num2str(Ym),', Name "Matrix/Young modulus [GPa]"];']);
fprintf(pro, '%s\n', ['PoissonM = DefineNumber[ ',num2str(Pm),', Name "Matrix/Poisson coefficient []"];']);
fprintf(pro, '%s\n', ['YoungF = 1e9 * DefineNumber[ ',num2str(Yf),', Name "Fiber/Young modulus [GPa]"];']);
fprintf(pro, '%s\n\n', ['PoissonF = DefineNumber[ ',num2str(Pf),', Name "Fiber/Poisson coefficient []"];']);
fprintf(pro, '%s\n\n', 'AppliedDisplacement = mm * DefineNumber[ 1, Name "BC/Applied displacement [mm]"];');
fprintf(pro, '%s\n\n\n\n', 'Scaling = DefineNumber[ 200, Name "Scaling for displacement/Factor []"];');

% Group
fprintf(pro, '%s\n', 'Group {');
fprintf(pro, '%s\n\n', '  // Physical regions: Give explicit labels to the regions defined in the .geo file');
fprintf(pro, '%s\n', '  Matrix = Region[1];');
fprintf(pro, '%s', '  Fiber = Region[{ 2');
for i=2:size(C,1)
    fprintf(pro, '%s', ',', num2str(i+1));
end
fprintf(pro, '%s\n', ' }];');
fprintf(pro, '%s', '  Whole = Region[{ 1');
for i=1:size(C,1)
    fprintf(pro, '%s', [',', num2str(i+1)]);
end
fprintf(pro, '%s\n', ' }];');
fprintf(pro, '%s\n', ['  LHS = Region[', num2str(size(C,1)+2),'];']);
fprintf(pro, '%s\n', ['  RHS = Region[', num2str(size(C,1)+3),'];']);
fprintf(pro, '%s\n\n', ['  CornerNode = Region [', num2str(size(C,1)+4),'];']);
fprintf(pro, '%s\n\n', '  // Abstract regions:');
fprintf(pro, '%s\n', '  Vol_Mec = Region[ Whole ];   // Elastic Domain');
fprintf(pro, '%s\n', '  Sur_Clamp_Mec = Region[ LHS ];   // Surface with imposed zero displacements');
fprintf(pro, '%s\n\n', '}');

% Function
fprintf(pro, '%s\n', 'Function {');
fprintf(pro, '%s\n', '  E[Matrix] = YoungM;');
fprintf(pro, '%s\n', '  E[Fiber] = YoungF;');
fprintf(pro, '%s\n', '  nu[Matrix] = PoissonM;');
fprintf(pro, '%s\n', '  nu[Fiber] = PoissonF;');
fprintf(pro, '%s\n\n', '}');

% Function
fprintf(pro, '%s\n', 'Function {');
fprintf(pro, '%s\n', '  Flag_EPC = 1;');
fprintf(pro, '%s\n', '  If(Flag_EPC) // Plane stress');
fprintf(pro, '%s\n', '    a[] = E[]/(1.-nu[]^2);');
fprintf(pro, '%s\n', '    c[] = E[]*nu[]/(1.-nu[]^2);');
fprintf(pro, '%s\n', '  Else // Plane strain or 3D');
fprintf(pro, '%s\n', '    a[] = E[]*(1.-nu[])/(1.+nu[])/(1.-2.*nu[]);');
fprintf(pro, '%s\n', '    c[] = E[]*nu[]/(1.+nu[])/(1.-2.*nu[]);');
fprintf(pro, '%s\n', '  EndIf');
fprintf(pro, '%s\n\n', '  b[] = E[]/2./(1.+nu[]);');
fprintf(pro, '%s\n', '  C_xx[] = Tensor[ a[],0  ,0  ,    0  ,b[],0  ,    0  ,0  ,b[] ];');
fprintf(pro, '%s\n\n', '  C_xy[] = Tensor[ 0  ,c[],0  ,    b[],0  ,0  ,    0  ,0  ,0   ];');
fprintf(pro, '%s\n', '  C_yx[] = Tensor[ 0  ,b[],0  ,    c[],0  ,0  ,    0  ,0  ,0   ];');
fprintf(pro, '%s\n', '  C_yy[] = Tensor[ b[],0  ,0  ,    0  ,a[],0  ,    0  ,0  ,b[] ];');
fprintf(pro, '%s\n\n', '}');

% Displacement Boundary Conditions
fprintf(pro, '%s\n', '// Displacement boundary conditions');
fprintf(pro, '%s\n', 'Constraint {');
fprintf(pro, '%s\n', '  { Name Displacement_x;');
fprintf(pro, '%s\n', '    Case {');
fprintf(pro, '%s\n', '      { Region Sur_Clamp_Mec ; Type Assign ; Value 0; }');
fprintf(pro, '%s\n', '      { Region RHS; Type Assign ; Value AppliedDisplacement; }');
fprintf(pro, '%s\n', '    }');
fprintf(pro, '%s\n', '  }');
fprintf(pro, '%s\n', '  { Name Displacement_y;');
fprintf(pro, '%s\n', '    Case {');
fprintf(pro, '%s\n', '       { Region CornerNode ; Type Assign ; Value 0; }');
fprintf(pro, '%s\n', '    }');
fprintf(pro, '%s\n', '  }');
fprintf(pro, '%s\n\n\n', '}');

% Domain of definition of the "ux" and "uy" FunctionSpaces
fprintf(pro, '%s\n', '// Domain of definition of the "ux" and "uy" FunctionSpaces');
fprintf(pro, '%s\n', 'Group {');
fprintf(pro, '%s\n', '  Dom_H_u_Mec = Region[ { Vol_Mec, Sur_Clamp_Mec} ];');
fprintf(pro, '%s\n\n', '}');
fprintf(pro, '%s\n', 'Flag_Degree = DefineNumber[ 0, Name "Geometry/Use degree 2", Choices{0,1}, Visible 1];');
fprintf(pro, '%s\n\n', 'FE_Order = ( Flag_Degree == 0 ) ? 1 : 2; // Convert flag value into polynomial degree');

% Function Space
fprintf(pro, '%s\n', 'FunctionSpace {');
fprintf(pro, '%s\n', '  { Name H_ux_Mec ; Type Form0 ;');
fprintf(pro, '%s\n', '    BasisFunction {');
fprintf(pro, '%s\n', '      { Name sxn ; NameOfCoef uxn ; Function BF_Node ;');
fprintf(pro, '%s\n', '        Support Dom_H_u_Mec ; Entity NodesOf[ All ] ; }');
fprintf(pro, '%s\n', '     If ( FE_Order == 2 )');
fprintf(pro, '%s\n', '        { Name sxn2 ; NameOfCoef uxn2 ; Function BF_Node_2E ;');
fprintf(pro, '%s\n', '          Support Dom_H_u_Mec; Entity EdgesOf[ All ] ; }');
fprintf(pro, '%s\n', '     EndIf');
fprintf(pro, '%s\n', '    }');
fprintf(pro, '%s\n', '    Constraint {');
fprintf(pro, '%s\n', '      { NameOfCoef uxn ;');
fprintf(pro, '%s\n', '        EntityType NodesOf ; NameOfConstraint Displacement_x ; }');
fprintf(pro, '%s\n', '      If ( FE_Order == 2 )');
fprintf(pro, '%s\n', '         { NameOfCoef uxn2 ;');
fprintf(pro, '%s\n', '	   EntityType EdgesOf ; NameOfConstraint Displacement_x ; }');
fprintf(pro, '%s\n', '     EndIf');
fprintf(pro, '%s\n', '    }');
fprintf(pro, '%s\n', '  }');
fprintf(pro, '%s\n', '  { Name H_uy_Mec ; Type Form0 ;');
fprintf(pro, '%s\n', '    BasisFunction {');
fprintf(pro, '%s\n', '      { Name syn ; NameOfCoef uyn ; Function BF_Node ;');
fprintf(pro, '%s\n', '        Support Dom_H_u_Mec ; Entity NodesOf[ All ] ; }');
fprintf(pro, '%s\n', '     If ( FE_Order == 2 )');
fprintf(pro, '%s\n', '        { Name syn2 ; NameOfCoef uyn2 ; Function BF_Node_2E ;');
fprintf(pro, '%s\n', '          Support Dom_H_u_Mec; Entity EdgesOf[ All ] ; }');
fprintf(pro, '%s\n', '     EndIf');
fprintf(pro, '%s\n', '    }');
fprintf(pro, '%s\n', '    Constraint {');
fprintf(pro, '%s\n', '      { NameOfCoef uyn ;');
fprintf(pro, '%s\n', '        EntityType NodesOf ; NameOfConstraint Displacement_y ; }');
fprintf(pro, '%s\n', '      If ( FE_Order == 2 )');
fprintf(pro, '%s\n', '      { NameOfCoef uyn2 ;');
fprintf(pro, '%s\n', '        EntityType EdgesOf ; NameOfConstraint Displacement_y ; }');
fprintf(pro, '%s\n', '      EndIf');
fprintf(pro, '%s\n', '    }');
fprintf(pro, '%s\n', '  }');
fprintf(pro, '%s\n\n\n', '}');

% Jacobian
fprintf(pro, '%s\n', 'Jacobian {');
fprintf(pro, '%s\n', '  { Name Vol;');
fprintf(pro, '%s\n', '    Case {');
fprintf(pro, '%s\n', '      { Region All; Jacobian Vol; }');
fprintf(pro, '%s\n', '    }');
fprintf(pro, '%s\n', '  }');
fprintf(pro, '%s\n', '  { Name Sur;');
fprintf(pro, '%s\n', '    Case {');
fprintf(pro, '%s\n', '      { Region All; Jacobian Sur; }');
fprintf(pro, '%s\n', '    }');
fprintf(pro, '%s\n', '  }');
fprintf(pro, '%s\n\n', '}');

% Integration
fprintf(pro, '%s\n', 'Integration {');
fprintf(pro, '%s\n', '  { Name Gauss_v;');
fprintf(pro, '%s\n', '    Case {');
fprintf(pro, '%s\n', '      If (FE_Order == 1)');
fprintf(pro, '%s\n', '      { Type Gauss;');
fprintf(pro, '%s\n', '        Case {');
fprintf(pro, '%s\n', '          { GeoElement Line       ; NumberOfPoints  3; }');
fprintf(pro, '%s\n', '          { GeoElement Triangle   ; NumberOfPoints  3; }');
fprintf(pro, '%s\n', '          { GeoElement Quadrangle ; NumberOfPoints  4; }');
fprintf(pro, '%s\n', '        }');
fprintf(pro, '%s\n', '      }');
fprintf(pro, '%s\n', '      Else');
fprintf(pro, '%s\n', '      { Type Gauss;');
fprintf(pro, '%s\n', '        Case {');
fprintf(pro, '%s\n', '	  { GeoElement Line       ; NumberOfPoints  5; }');
fprintf(pro, '%s\n', '          { GeoElement Triangle   ; NumberOfPoints  7; }');
fprintf(pro, '%s\n', '          { GeoElement Quadrangle ; NumberOfPoints  7; }');
fprintf(pro, '%s\n', '        }');
fprintf(pro, '%s\n', '      }');
fprintf(pro, '%s\n', '      EndIf');
fprintf(pro, '%s\n', '    }');
fprintf(pro, '%s\n', '  }');
fprintf(pro, '%s\n\n', '}');

% Formulation
fprintf(pro, '%s\n', 'Formulation {');
fprintf(pro, '%s\n', '  { Name Elast_u ; Type FemEquation ;');
fprintf(pro, '%s\n', '    Quantity {');
fprintf(pro, '%s\n', '      { Name ux  ; Type Local ; NameOfSpace H_ux_Mec ; }');
fprintf(pro, '%s\n', '      { Name uy  ; Type Local ; NameOfSpace H_uy_Mec ; }');
fprintf(pro, '%s\n', '    }');
fprintf(pro, '%s\n', '    Equation {');
fprintf(pro, '%s\n', '      Integral { [ -C_xx[] * Dof{d ux}, {d ux} ] ;');
fprintf(pro, '%s\n', '        In Vol_Mec ; Jacobian Vol ; Integration Gauss_v ; }');
fprintf(pro, '%s\n', '      Integral { [ -C_xy[] * Dof{d uy}, {d ux} ] ;');
fprintf(pro, '%s\n', '        In Vol_Mec ; Jacobian Vol ; Integration Gauss_v ; }');
fprintf(pro, '%s\n', '      Integral { [ -C_yx[] * Dof{d ux}, {d uy} ] ;');
fprintf(pro, '%s\n', '        In Vol_Mec ; Jacobian Vol ; Integration Gauss_v ; }');
fprintf(pro, '%s\n', '      Integral { [ -C_yy[] * Dof{d uy}, {d uy} ] ;');
fprintf(pro, '%s\n', '        In Vol_Mec ; Jacobian Vol ; Integration Gauss_v ; }');
fprintf(pro, '%s\n', '    }');
fprintf(pro, '%s\n', '  }');
fprintf(pro, '%s\n\n', '}');

% Resolution
fprintf(pro, '%s\n', 'Resolution {');
fprintf(pro, '%s\n', '  { Name Elast_u ;');
fprintf(pro, '%s\n', '    System {');
fprintf(pro, '%s\n', '      { Name Sys_Mec ; NameOfFormulation Elast_u; }');
fprintf(pro, '%s\n', '    }');
fprintf(pro, '%s\n', '    Operation {');
fprintf(pro, '%s\n', '      InitSolution [Sys_Mec];');
fprintf(pro, '%s\n', '      Generate[Sys_Mec];');
fprintf(pro, '%s\n', '      Solve[Sys_Mec];');
fprintf(pro, '%s\n', '      SaveSolution[Sys_Mec] ;');
fprintf(pro, '%s\n', '    }');
fprintf(pro, '%s\n', '  }');
fprintf(pro, '%s\n\n', '}');

% Post Processing
fprintf(pro, '%s\n', 'PostProcessing {');
fprintf(pro, '%s\n', '  { Name Elast_u ; NameOfFormulation Elast_u ;');
fprintf(pro, '%s\n', '    PostQuantity {');
fprintf(pro, '%s\n', '      { Name u ; Value {');
fprintf(pro, '%s\n', '          Term { [ Scaling*Vector[ {ux}, {uy}, 0 ]]; In Vol_Mec ; Jacobian Vol ; }');
fprintf(pro, '%s\n', '        }');
fprintf(pro, '%s\n', '      }');
fprintf(pro, '%s\n', '      { Name sig_xx ; Value {');
fprintf(pro, '%s\n', '          Term { [ CompX[  C_xx[]*{d ux} + C_xy[]*{d uy} ] ];');
fprintf(pro, '%s\n', '            In Vol_Mec ; Jacobian Vol ; }');
fprintf(pro, '%s\n', '        }');
fprintf(pro, '%s\n', '      }');
fprintf(pro, '%s\n', '      { Name sig_xy ; Value {');
fprintf(pro, '%s\n', '          Term { [ CompY[  C_xx[]*{d ux} + C_xy[]*{d uy} ] ];');
fprintf(pro, '%s\n', '            In Vol_Mec ; Jacobian Vol ; }');
fprintf(pro, '%s\n', '        }');
fprintf(pro, '%s\n', '      }');
fprintf(pro, '%s\n', '      { Name sig_yy ; Value {');
fprintf(pro, '%s\n', '          Term { [  CompY [ C_yx[]*{d ux} + C_yy[]*{d uy} ] ];');
fprintf(pro, '%s\n', '            In Vol_Mec ; Jacobian Vol ; }');
fprintf(pro, '%s\n', '        }');
fprintf(pro, '%s\n', '      }');
fprintf(pro, '%s\n', '    }');
fprintf(pro, '%s\n', '  }');
fprintf(pro, '%s\n\n', '}');

% Post Operation
fprintf(pro, '%s\n', 'PostOperation {');
fprintf(pro, '%s\n', '  { Name pos; NameOfPostProcessing Elast_u;');
fprintf(pro, '%s\n', '    Operation {');
fprintf(pro, '%s\n', '      If(FE_Order == 1)');
fprintf(pro, '%s\n', '	Print[ sig_xx, OnElementsOf Whole, File "sigxx.pos" ];');
fprintf(pro, '%s\n', '	Print[ sig_xy, OnElementsOf Whole, File "sigxy.pos" ];');
fprintf(pro, '%s\n', '        Print[ u, OnElementsOf Whole, File "u.pos" ];');
fprintf(pro, '%s\n', '      Else');
fprintf(pro, '%s\n', '	Print[ sig_xx, OnElementsOf Whole, File "sigxx2.pos" ];');
fprintf(pro, '%s\n', '	Print[ sig_xy, OnElementsOf Whole, File "sigxy2.pos" ];');
fprintf(pro, '%s\n', '        Print[ u, OnElementsOf Whole, File "u2.pos" ];');
fprintf(pro, '%s\n', '      EndIf');
fprintf(pro, '%s\n', '      Echo[ StrCat["l=PostProcessing.NbViews-1; ",');
fprintf(pro, '%s\n', '      		   "View[l].VectorType = 5; ",');
fprintf(pro, '%s\n', '      		   "View[l].ExternalView = l; ",');
fprintf(pro, '%s\n', '      		   "View[l-1].IntervalsType = 3; "');
fprintf(pro, '%s\n', '		   ],');
fprintf(pro, '%s\n', '             File "tmp.geo", LastTimeStepOnly] ;');
fprintf(pro, '%s\n', '    }');
fprintf(pro, '%s\n', '  }');
fprintf(pro, '%s\n\n', '}');

fprintf(pro, '%s\n', '// Tell Gmsh which GetDP commands to execute when running the model');
fprintf(pro, '%s\n', 'DefineConstant[');
fprintf(pro, '%s\n', '  R_ = {"Elast_u", Name "GetDP/1ResolutionChoices", Visible 0},');
fprintf(pro, '%s\n', '  P_ = {"pos", Name "GetDP/2PostOperationChoices", Visible 0},');
fprintf(pro, '%s\n', '  C_ = {"-solve -pos -v2 ", Name "GetDP/9ComputeCommand", Visible 0}');
fprintf(pro, '%s\n\n\n', '];');

fprintf(pro, '%s\n', '// Author: Andrea Geremia   |   Copyright (c) [2020] [Andrea Geremia]   |   https://gitlab.com/agere35');
fclose(pro);

%% RESULTS ILLUSTRATION

% Plot of Domain
grid on
hold on
axis equal
axis([0 l 0 l])
title('COMPOSITE WITH RANDOM FIBRE DISTRIBUTION FOR GMSH ','Color','r');

% Plot of Fibers
t = 0:0.01:2*pi;
for i = 1:size(C,1)
    plot(C(i,1)+r*cos(t),C(i,2)+r*sin(t),'-k')
end

%%
disp('%%%%%%%% PROCESS ENDED %%%%%%%%');
disp('Open "composite_rve.geo" or "composite_rve.pro" on Gmsh');
toc

%% Author: Andrea Geremia   |   Copyright (c) [2020] [Andrea Geremia]   |   https://gitlab.com/agere35